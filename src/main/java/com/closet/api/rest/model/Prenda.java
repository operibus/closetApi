package com.closet.api.rest.model;

import java.util.Date;

/**
 * Created by jalberto.munoz on 21/04/2016.
 */
public class Prenda {
    private Long id;
    private String foto;
    private String descripcion;
    private Date fechaCreacion;
    private Long tallaId;
    private Long marcaId;
    private Long tipoPrendaOfrecida;
    private Long tipoPrendaId;
    private Long usuarioId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getTallaId() {
        return tallaId;
    }

    public void setTallaId(Long tallaId) {
        this.tallaId = tallaId;
    }

    public Long getMarcaId() {
        return marcaId;
    }

    public void setMarcaId(Long marcaId) {
        this.marcaId = marcaId;
    }

    public Long getTipoPrendaOfrecida() {
        return tipoPrendaOfrecida;
    }

    public void setTipoPrendaOfrecida(Long tipoPrendaOfrecida) {
        this.tipoPrendaOfrecida = tipoPrendaOfrecida;
    }

    public Long getTipoPrendaId() {
        return tipoPrendaId;
    }

    public void setTipoPrendaId(Long tipoPrendaId) {
        this.tipoPrendaId = tipoPrendaId;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

}
