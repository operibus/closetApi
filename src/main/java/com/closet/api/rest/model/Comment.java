package com.closet.api.rest.model;

import java.sql.Date;

/**
 * Created by Albert on 30/05/2016.
 */
public class Comment {
    private Long id;
    private String texto;
    private Date fecha;
    private String usuarioId;
    private Long prendaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getPrendaId() {
        return prendaId;
    }

    public void setPrendaId(Long prendaId) {
        this.prendaId = prendaId;
    }
}
