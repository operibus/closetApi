package com.closet.api.rest.model;

import java.util.Collection;

/**
 * Created by jalberto.munoz on 21/04/2016.
 */

public class EstadoIntercambio {
    private Integer id;
    private String nombre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
