package com.closet.api.rest.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by jalberto.munoz on 21/04/2016.
 */
public class Notificacion implements Serializable {
    private Long id;
    private String descripcion;
    private Long usuarioId;
    private Long tipoNotificacionId;
    private boolean leida;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getTipoNotificacionId() {
        return tipoNotificacionId;
    }

    public void setTipoNotificacionId(Long tipoNotificacionId) {
        this.tipoNotificacionId = tipoNotificacionId;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;}
    public void setLeida(Boolean leida) {
        this.leida = leida;
    }

    public Boolean getLeida() {
        return leida;
    }
}
