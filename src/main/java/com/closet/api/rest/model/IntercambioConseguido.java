package com.closet.api.rest.model;

import java.sql.Date;
import java.util.Collection;

/**
 * Created by jalberto.munoz on 21/04/2016.
 */

public class IntercambioConseguido {
    private Long id;
    private Long estadoIntercambioId;
    private Long usuarioId;
    private Date fecha;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEstadoIntercambioId() {
        return estadoIntercambioId;
    }

    public void setEstadoIntercambioId(Long estadoIntercambioId) {
        this.estadoIntercambioId = estadoIntercambioId;
    }


    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
