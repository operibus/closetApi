package com.closet.api.rest.model;

import java.sql.Date;

/**
 * Created by jalberto.munoz on 21/04/2016.
 */
public class Comentario {
    private Long id;
    private String texto;
    private Date fecha;
    private Long usuarioId;
    private Long prendaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getPrendaId() {
        return prendaId;
    }

    public void setPrendaId(Long prendaId) {
        this.prendaId = prendaId;
    }

}
