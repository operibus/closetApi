package com.closet.api.rest.model;

import java.util.List;

/**
 * Created by Albert on 18/05/2016.
 */
public class AutoComplete {

    private List<Provincia> provincias;
    private List<Usuario> usuarios;
    private List<Talla> tallas;
    private List<TipoPrenda> tipoPrenda;


    public List<Provincia> getProvincias() {
        return provincias;
    }

    public void setProvincias(List<Provincia> provincias) {
        this.provincias = provincias;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Talla> getTallas() {
        return tallas;
    }

    public void setTallas(List<Talla> tallas) {
        this.tallas = tallas;
    }

    public List<TipoPrenda> getTipoPrenda() {
        return tipoPrenda;
    }

    public void setTipoPrenda(List<TipoPrenda> tipoPrenda) {
        this.tipoPrenda = tipoPrenda;
    }
}
