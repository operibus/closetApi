package com.closet.api.rest.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by jalberto.munoz on 21/04/2016.
 */
public class TipoPrenda implements Serializable {
    private Long id;
    private String nombre;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
