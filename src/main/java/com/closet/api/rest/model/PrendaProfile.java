package com.closet.api.rest.model;

import java.util.Date;

/**
 * Created by Albert on 24/05/2016.
 */
public class PrendaProfile{
    private Long id;
    private String foto;
    private String descripcion;
    private String fechaCreacion;
    private String tallaId;
    private String marca;
    private String tipoPrendaOfrecida;
    private String tipoPrenda;
    private String usuario;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getTallaId() {
        return tallaId;
    }

    public void setTallaId(String tallaId) {
        this.tallaId = tallaId;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipoPrendaOfrecida() {
        return tipoPrendaOfrecida;
    }

    public void setTipoPrendaOfrecida(String tipoPrendaOfrecida) {
        this.tipoPrendaOfrecida = tipoPrendaOfrecida;
    }

    public String getTipoPrenda() {
        return tipoPrenda;
    }

    public void setTipoPrenda(String tipoPrenda) {
        this.tipoPrenda = tipoPrenda;
    }
}
