package com.closet.api.rest.model;

import java.sql.Date;
import java.util.Collection;

/**
 * Created by jalberto.munoz on 21/04/2016.
 */
public class Intercambio {
    private Long id;
    private Date fechaInicio;
    private Long usuarioId;
    private Long estadoIntercambioId;
    private Long usuario2Id;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getEstadoIntercambioId() {
        return estadoIntercambioId;
    }

    public void setEstadoIntercambioId(Long estadoIntercambioId) {
        this.estadoIntercambioId = estadoIntercambioId;
    }

    public Long getUsuario2Id() {
        return usuario2Id;
    }

    public void setUsuario2Id(Long usuario2Id) {
        this.usuario2Id = usuario2Id;
    }

}
