package com.closet.api.rest.resource;

import com.closet.api.rest.error.AppException;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.Provincia;
import com.closet.api.rest.controller.FormController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("/provincia")

public class FormResource {

    /*Points the bean where the controller is located*/
    @Autowired
    private FormController formController;

    /*Gets all the Spanish provinces*/
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProvincia() throws AppException {
        List<Provincia> provincias = formController.getProvincias();
        return Response.status(200)
                .entity(provincias)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    /*Gets all the cities based on the provinces id*/
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCitiesByProvincia(@PathParam("id") String id) throws AppException {
        Ciudad ciudad = new Ciudad();
        ciudad.setId(Long.parseLong(id));
        List<Ciudad> ciudades = formController.getCitiesByProvincia(ciudad.getId());
        return Response.status(200)
                .entity(ciudades)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }
}
