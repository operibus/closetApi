package com.closet.api.rest.resource;

import com.closet.api.rest.controller.AutoCompleteControl;
import com.closet.api.rest.error.AppException;
import com.closet.api.rest.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Albert on 18/05/2016.
 */
@Component
@Path("/autocomplete")
public class AutocompleteResource {

    @Autowired
    private AutoCompleteControl autoCompleteController;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAll() throws AppException {
        AutoComplete autoComplete = autoCompleteController.getAll();
        return Response.status(200)
                .entity(autoComplete)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    @GET
    @Path("/search/{nombre}/{tipo}/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getResults(@PathParam("nombre") String nombre,
                               @PathParam("tipo")String tipo,
                               @PathParam("id")String id) throws AppException {
        if (tipo.equals("tipoPrenda")) {
            List<PrendaProfile> prendas = autoCompleteController.getPrendaByTipo(id);
            return Response.status(200)
                    .entity(prendas)
                    .header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();

        } else if (tipo.equals("provincia")) {
            List<Usuario> usuarios = autoCompleteController.getUserByProvince(id);
            return Response.status(200)
                    .entity(usuarios)
                    .header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();
        } else if (tipo.equals("usuario")) {
            List<Profile> usuarios = autoCompleteController.getUserById(id);
            return Response.status(200)
                    .entity(usuarios)
                    .header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();
        } else if (tipo.equals("talla")) {
            List<Prenda> prendas = autoCompleteController.getprendaByTalla(nombre);
            return Response.status(200)
                    .entity(prendas)
                    .header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();
        }else{
            return Response.status(400).header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();
        }
    }
}
