package com.closet.api.rest.resource;

import com.closet.api.rest.controller.ImageController;
import com.closet.api.rest.controller.ProfileController;
import com.closet.api.rest.controller.UsuarioController;
import com.closet.api.rest.error.AppException;
import com.closet.api.rest.model.Prenda;
import com.closet.api.rest.model.PrendaProfile;
import com.closet.api.rest.model.Profile;
import com.closet.api.rest.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jalberto.munoz on 02/05/2016.
 */
@Component
@Path("/perfil")
public class ProfileResource {


        /*Points the bean where the controller is located*/
        @Autowired
        private ProfileController profileController;

        @Autowired
        private ImageController imageController;

        /*Gets all the Spanish provinces*/
        @GET
        @Path("/prendas/{id}")
        @Produces({MediaType.APPLICATION_JSON})
        public Response findPrendaById(@PathParam("id") String id) throws AppException {
                List<PrendaProfile> prendas2 = new ArrayList<PrendaProfile>();
                try{
                   List<PrendaProfile> prendas = profileController.getPrendasByUserId(id);
                        prendas2 = prendas;
                }catch(Exception e){

                }

            return Response.status(200)
                    .entity(prendas2)
                    .header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();
        }

        @GET
        @Path("{id}")
        @Produces({MediaType.APPLICATION_JSON})
        public Response findUserById(@PathParam("id") String id) throws AppException {
                Profile userProfile = profileController.getUser(id);
                return Response.status(200)
                        .entity(userProfile)
                        .header("Access-Control-Allow-Headers", "X-extra-header")
                        .allow("OPTIONS").build();
        }

        @POST
        @Path("foto")
        @Produces({MediaType.APPLICATION_JSON})
        public Response changePhoto(Usuario user) throws AppException {
                byte[] photo = user.getFoto().getBytes();
                imageController.putImageProfile(photo, user.getId());
                return Response.status(200)
                        .header("Access-Control-Allow-Headers", "X-extra-header")
                        .allow("OPTIONS").build();
        }
}
