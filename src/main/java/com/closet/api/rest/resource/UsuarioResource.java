package com.closet.api.rest.resource;

import com.closet.api.rest.error.AppException;
import com.closet.api.rest.model.Profile;
import com.closet.api.rest.model.Usuario;
import com.closet.api.rest.controller.UsuarioController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by jalberto.munoz on 29/04/2016.
 */
@Component
@Path("/usuario")
public class UsuarioResource {

    /*Spring points where is located the controller*/
    @Autowired
    private UsuarioController usuarioController;

    /*Gets all usuarios*/
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllUsers() throws AppException {
        List<Usuario> usuarios = usuarioController.getAllUsers();
        return Response.status(200)
                .entity(usuarios)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    /**
     * Checks if the users exists, if not return null, if exists then returns the user so as to login
     * */
    @GET
    @Path("/login/{email}/{contrasena}")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getUserByEmail(@PathParam("email") String email, @PathParam("contrasena")String contrasena ) throws AppException {
        Usuario usuario2 = new Usuario();
        usuario2.setNombre("No existe");
        Boolean existe = usuarioController.existEmail(email);
        if (existe) {
            Profile usuario = usuarioController.getUserByEmail(email, contrasena);
            return Response.status(200)
                    .entity(usuario)
                    .header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();
        } else {
            return Response.status(201).entity(usuario2)
                    .header("Access-Control-Allow-Headers", "X-extra-header")
                    .allow("OPTIONS").build();

        }
    }

    /*Adds a new user*/
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addNewUser(Usuario usuario) throws AppException {
        usuario = usuarioController.addNewUser(usuario);

        return Response.status(200)
                .entity(usuario)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    /*Modifies a user*/
    @POST
    @Path("/update")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateUser(Usuario usuario) throws AppException {
       usuarioController.updateUser(usuario);
        return Response.status(200)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    /*Deletes a user*/
    @DELETE
    @Path("/delete/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response deletesUser(@PathParam("id") String id) throws AppException{
        usuarioController.deleteUser(Long.parseLong(id));
        return Response.status(200)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }


    /*Find users by city*/
    @GET
    @Path("/ciudad/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response findUsersByCity(@PathParam("id") String id) throws AppException {
        List<Usuario> usuarios = usuarioController.getUsersByCity(id);
        return Response.status(200)
                .entity(usuarios)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();

    }

    @GET
    @Path("/getAll")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response findAllUsers() throws AppException {
        List<Usuario> usuarios = usuarioController.getAllUsers();
        return Response.status(200)
                .entity(usuarios)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();

    }

}
