package com.closet.api.rest.resource;

import com.closet.api.rest.controller.GarmentController;
import com.closet.api.rest.controller.ImageController;
import com.closet.api.rest.error.AppException;
import com.closet.api.rest.model.*;
import com.sun.org.apache.xpath.internal.SourceTree;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.*;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by Albert on 25/05/2016.
 */

@Component
@Path("/newGarment")
public class GarmentResource {


    @Autowired
    private GarmentController garmentController;
    @Autowired
    private ImageController imageController;

    @GET
    @Path("/tallas")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllSizes() throws AppException {
        List<Talla> tallas = garmentController.getTallas();
        return Response.status(200)
                .entity(tallas)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    @GET
    @Path("/prendas")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllTypes() throws AppException {
        List<TipoPrenda> tipos = garmentController.getTipoPrenda();
        return Response.status(200)
                .entity(tipos)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    @GET
    @Path("/marcas")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllBrands() throws AppException {
        List<Marca> marcas = garmentController.getMarca();

        return Response.status(200)
                .entity(marcas)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    @POST
    @Path("/image")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getImages(byte[] file) throws AppException{
        imageController.putImageGarment(file);
        byte[] data = Base64.decodeBase64(file);
        try (OutputStream stream = new FileOutputStream("E:/imagen.jpg")) {
            stream.write(data);
        }catch(Exception e){

        }
        return Response.status(200)
                .entity(file)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }


    @POST
    @Path("/garment")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getImages(Prenda prenda) throws AppException{
        garmentController.postGarment(prenda);
        return Response.status(200)
                .entity(prenda)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    @GET
    @Path("/comments/{prendaId}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getComments(@PathParam("prendaId") String prendaId) throws AppException{
       List <Comment> comment = garmentController.getCommentsByGarmentId(prendaId);
        return Response.status(200)
                .entity(comment)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    @POST
    @Path("/comments/{usuarioId}/{prendaId}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response insertComments(Comentario comentario) throws AppException{
        garmentController.insertComment(comentario);
        return Response.status(200)
                .entity(comentario)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }

    @DELETE
    @Path("/delete/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response deletesUser(@PathParam("id") String id) throws AppException{
        garmentController.deleteGarment(Long.parseLong(id));
        return Response.status(200)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }
}
