package com.closet.api.rest.resource;

import com.closet.api.rest.controller.MessageController;
import com.closet.api.rest.error.AppException;
import com.closet.api.rest.model.Conversacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by jalberto.munoz on 03/05/2016.
 */

@Component
@Path("/conversacion")
public class MessageResource {
    /*Points the bean where the controller is located*/
    @Autowired
    private MessageController messageController;

    /*Gets all the messages from a conversation*/
    @GET
    @Path("{conversacion_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getConversation(@PathParam("conversacion_id") String conversacionId) throws AppException {
        List<Conversacion> conversaciones = messageController.getConversacionesByUserId(conversacionId);
        return Response.status(200)
                .entity(conversaciones)
                .header("Access-Control-Allow-Headers", "X-extra-header")
                .allow("OPTIONS").build();
    }
}
