package com.closet.api.rest.dao.implementation;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.interfaces.ProvinciaDao;
import com.closet.api.rest.model.Provincia;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jalberto.munoz on 25/04/2016.
 */
public class JDBCProvinciaDao implements ProvinciaDao {
//    Select * from ciudad, provincia where ciudad.provincia_id = provincia.id;

        // Constants ----------------------------------------------------------------------------------

        private static final String SQL_FIND =
                "Select * from provincia";
        private static final String SQL_FIND_PROVINCIA_BY_ID="Select * from provincia where id=";

        // Vars ---------------------------------------------------------------------------------------

        private DaoFactory daoFactory;

        // Constructors -------------------------------------------------------------------------------

        /**
         * Construct an User DAO for the given DAOFactory. Package private so that it can be constructed
         * inside the DAO package only.
         * @param daoFactory The DAOFactory to construct this User DAO for.
         */
        JDBCProvinciaDao(DaoFactory daoFactory) {
            this.daoFactory = daoFactory;
        }

        // Actions ------------------------------------------------------------------------------------
        @Override
    public  List<Provincia> list() throws DaoException {
            List<Provincia> provincias = new ArrayList<>();
            try (
                    Connection connection = daoFactory.getConnection();
                    PreparedStatement statement = connection.prepareStatement(SQL_FIND);
                    ResultSet resultSet = statement.executeQuery();
            ) {

                while (resultSet.next()) {
                    provincias.add(map(resultSet));
                }

            } catch (SQLException e) {
                throw new DaoException(e);
            }

            return provincias;
        }

    public Provincia getProvinciaById(Long id) throws DaoException {
        Provincia provincia = new Provincia();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_PROVINCIA_BY_ID+id);
                ResultSet resultSet = statement.executeQuery();
        ) {

            while (resultSet.next()) {
                provincia = map(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return provincia;
    }

    private Provincia map(ResultSet resultSet) throws SQLException {
        Provincia provincia = new Provincia();
        provincia.setId(resultSet.getLong("id"));
        provincia.setNombre(resultSet.getString("nombre"));
        return provincia;
    }

}
