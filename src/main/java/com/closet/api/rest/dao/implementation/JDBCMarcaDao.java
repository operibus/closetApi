package com.closet.api.rest.dao.implementation;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.interfaces.MarcaDao;
import com.closet.api.rest.model.Marca;
import com.closet.api.rest.model.Provincia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albert on 24/05/2016.
 */
public class JDBCMarcaDao implements MarcaDao {

    private static final String SQL_FIND_MARCA = "Select * from marca where id=";

    private static final String SQL_FIND_ALL_MARCAS = "Select * from marca";

    private DaoFactory daoFactory;

    JDBCMarcaDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Marca getMarcaById(Long id) throws DaoException {
        Marca marca = new Marca();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_MARCA+id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                marca = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return marca;
    }

    @Override
    public List<Marca> list() throws DaoException {
        List<Marca> marcas = new ArrayList<>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_MARCAS);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                marcas.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return marcas;
    }


    private Marca map(ResultSet resultSet) throws SQLException {
        Marca marca = new Marca();
        marca.setId(resultSet.getLong("id"));
        marca.setNombre(resultSet.getString("nombre"));
        return marca;
    }

}
