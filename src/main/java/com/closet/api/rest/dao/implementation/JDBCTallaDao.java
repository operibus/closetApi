package com.closet.api.rest.dao.implementation;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.interfaces.TallaDao;
import com.closet.api.rest.model.Provincia;
import com.closet.api.rest.model.Talla;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albert on 19/05/2016.
 */
public class JDBCTallaDao implements TallaDao {

    private static final String SQL_FIND_SIZE =
            "Select * from talla";
    private static final String SQL_FIND_SIZE_BY_ID =
            "Select * from talla where id=";

    private DaoFactory daoFactory;

    JDBCTallaDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public List<Talla> list(){
        List<Talla> tallas = new ArrayList<>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_SIZE);
                ResultSet resultSet = statement.executeQuery();
        ) {

            while (resultSet.next()) {
                tallas.add(map(resultSet));
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return tallas;
    }

    @Override
    public Talla getTallaById(Long id){
        Talla talla = new Talla();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_SIZE_BY_ID+id);
                ResultSet resultSet = statement.executeQuery();
        ) {

            while (resultSet.next()) {
                talla  = map(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return talla;
    }
    private Talla map(ResultSet resultSet) throws SQLException {
        Talla talla = new Talla();
        talla.setId(resultSet.getLong("id"));
        talla.setNombre(resultSet.getString("nombre"));
        return talla;
    }
}
