package com.closet.api.rest.dao.interfaces;
import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.Provincia;

import java.util.List;
/**
 * Created by Albert on 25/04/2016.
 */
public interface CiudadDao {
   List <Ciudad> getAllCitiesForProvincia(Long id) throws DaoException;

   Ciudad getCiudadById(Long id) throws DaoException;
}
