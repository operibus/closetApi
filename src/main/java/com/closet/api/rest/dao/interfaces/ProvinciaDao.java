package com.closet.api.rest.dao.interfaces;
import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.model.Provincia;

import java.util.List;

/**
 * Created by jalberto.munoz on 25/04/2016.
 */
public interface ProvinciaDao {
    List<Provincia> list() throws DaoException;

    Provincia getProvinciaById(Long id);

}
