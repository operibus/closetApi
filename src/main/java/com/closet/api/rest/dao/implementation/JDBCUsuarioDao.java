package com.closet.api.rest.dao.implementation;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.interfaces.UsuarioDao;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.Usuario;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static com.closet.api.rest.dao.implementation.DaoUtil.prepareStatement;


/**
 * Created by jalberto.munoz on 27/04/2016.
 */
public class JDBCUsuarioDao implements UsuarioDao {

    private static final String SQL_FIND_USERS = "Select * from usuario";

    private static final String SQL_FIND_USERS_BY_ID ="Select * from usuario where id=";

    private static final String SQL_FIND_USERS_BY_CITY = "Select * from usuario where ciudad_id= ";

    private static final String SQL_FIND_BY_EMAIL_AND_PASSWORD =
            "SELECT * FROM usuario WHERE email = ? AND contrasena = ?";
    private static final String SQL_CHANGE_PASSWORD =
            "UPDATE usuario SET contrasena = ? WHERE id = ?";
    private static final String SQL_EXIST_EMAIL =
            "SELECT id FROM usuario WHERE email = ?";
    private static final String SQL_DELETE =
            "DELETE FROM usuario WHERE id = ?";
    private static final String SQL_UPDATE =
            "UPDATE usuario SET email = ?, nombre = ?, ciudad_id = ? WHERE id = ?";
    private static final String SQL_INSERT =
            "INSERT INTO Usuario (nombre, email, fecha_creacion, contrasena, ciudad_id, tipo_usuario_id) " +
                    "VALUES (?, ?, NOW(), ?, ?, ?)";

    private static final String SQL_FIND_ID = "Select id FROM usuario where nombre=";

    private static final String INSERT_UPDATE_PHOTO_PATH = "UPDATE usuario SET foto = ?  WHERE id = ?";

    private DaoFactory daoFactory;

    JDBCUsuarioDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public List<Usuario> usersFind() throws DaoException {
        List<Usuario> usuarios = new ArrayList<>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_USERS);
                ResultSet resultSet = statement.executeQuery();
        ) {

            while (resultSet.next()) {
                usuarios.add(map(resultSet));
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return usuarios;
    }
    @Override
    public Usuario getUserById(Long id) throws DaoException {
        Usuario usuario = new Usuario();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_USERS_BY_ID+id);
                ResultSet resultSet = statement.executeQuery();
        ) {

            while (resultSet.next()) {
                usuario = map(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return usuario;
    }

    @Override
    public Usuario findUserByEmailPass(Usuario usuario) throws DaoException {
        Object[] values = {
                usuario.getEmail(),
                usuario.getContrasena()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_FIND_BY_EMAIL_AND_PASSWORD, true, values);
                ResultSet resultSet = statement.executeQuery();
        ) {

            if (resultSet.next()) {
                usuario = map(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return usuario;
    }
    @Override
    public List<Usuario> usersFindByCity(Long id)throws DaoException{
        List<Usuario> usuarios = new ArrayList<>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_USERS_BY_CITY+id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                usuarios.add(map(resultSet));
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return usuarios;
    }
    @Override
    public void create(Usuario usuario) throws DaoException{

        Object[] values = {
                usuario.getNombre(),
                usuario.getEmail(),
                usuario.getContrasena(),
                usuario.getCiudadId(),
                usuario.getTipoUsuarioId()
        };
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_INSERT, true, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DaoException("Creating user failed, no rows affected.");
            }
        }catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(Usuario usuario) throws DaoException {
        Object[] values = {
                usuario.getId()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_DELETE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DaoException("No se ha podido borrar el usuario");
            } else {
                usuario.setId(null);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
    @Override
    public void update(Usuario usuario) throws DaoException {
        if (usuario.getId() == null) {
            throw new IllegalArgumentException("El usuario no se ha creado aún.");
        }

        Object[] values = {
                usuario.getEmail(),
                usuario.getNombre(),
                usuario.getCiudadId(),
                usuario.getId()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DaoException("Fallo en la actualización.");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public boolean existEmail(String email) throws DaoException {
        Object[] values = {
                email
        };
        boolean exist = false;

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_EXIST_EMAIL, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            exist = resultSet.next();
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return exist;
    }

    @Override
    public List<Usuario> getUsersByProvincia(List<Ciudad> ciudades){
        List<Usuario> usuarios = new ArrayList<>();
        for(int i=0; i<ciudades.size();i++) {
            try (
                    Connection connection = daoFactory.getConnection();
                    PreparedStatement statement = connection.prepareStatement(SQL_FIND_USERS_BY_CITY + ciudades.get(i).getId());
                    ResultSet resultSet = statement.executeQuery();
            ) {
                while (resultSet.next()) {
                    usuarios.add(map(resultSet));
                }

            } catch (SQLException e) {
                throw new DaoException(e);
            }
        }
        return usuarios;
    }

    @Override
    public void updatePhoto(String photoPath, Long id){
        Object[] values = {
                photoPath, id
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, INSERT_UPDATE_PHOTO_PATH, true, values);
        ) {

            statement.executeUpdate();
        }catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    /*Maps the returned values from the database*/
    private Usuario map(ResultSet resultSet) throws SQLException {
        Usuario usuario = new Usuario();
        usuario.setId(resultSet.getLong("id"));
        usuario.setNombre(resultSet.getString("nombre"));
        usuario.setContrasena(resultSet.getString("contrasena"));
        usuario.setEmail(resultSet.getString("email"));
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
        Date fechaCreacion = df.parse(resultSet.getString("fecha_creacion"));
            usuario.setFechaCreacion(fechaCreacion);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        usuario.setCiudadId(resultSet.getLong("ciudad_id"));
        usuario.setTipoUsuarioId(resultSet.getLong("tipo_usuario_id"));
        return usuario;
    }

}
