package com.closet.api.rest.dao.interfaces;

import com.closet.api.rest.model.Conversacion;

import java.util.List;

/**
 * Created by jalberto.munoz on 03/05/2016.
 */
public interface ConversacionDao {

     List<Conversacion> getConversacionesByUserId(Long id);

//    Conversacion getConversacionesBy(Long id);

}
