package com.closet.api.rest.dao.implementation;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.interfaces.TipoPrendaDao;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.TipoPrenda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albert on 18/05/2016.
 */
public class JDBCTipoPrendaDao implements TipoPrendaDao {

    private static final String SQL_FIND_TIPOS_DE_PRENDA =
            "Select * from tipo_prenda";
    private static final String SQL_FIND_TIPO_BY_ID =
            "Select * from tipo_prenda where id=";
    private DaoFactory daoFactory;

    JDBCTipoPrendaDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    @Override
    public List<TipoPrenda> list() throws DaoException {
        List<TipoPrenda> tipos = new ArrayList<TipoPrenda>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_TIPOS_DE_PRENDA);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                tipos.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return tipos;
    }

    public TipoPrenda getTipoPrendaById(Long id) throws DaoException {
        TipoPrenda tipoPrenda = new TipoPrenda();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_TIPO_BY_ID+id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                tipoPrenda= map(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return tipoPrenda;

    }
    private TipoPrenda map(ResultSet resultSet) throws SQLException {
        TipoPrenda tipo = new TipoPrenda();
        tipo.setNombre(resultSet.getString("nombre"));
        tipo.setId(resultSet.getLong("id"));
        return tipo;
    }
}
