package com.closet.api.rest.dao.interfaces;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.model.TipoPrenda;

import java.util.List;

/**
 * Created by Albert on 18/05/2016.
 */
public interface TipoPrendaDao {
    List<TipoPrenda> list() throws DaoException;

    TipoPrenda getTipoPrendaById(Long id) throws DaoException;
}
