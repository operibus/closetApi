package com.closet.api.rest.dao.implementation;


import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.interfaces.PrendaDao;
import com.closet.api.rest.model.Comentario;
import com.closet.api.rest.model.Prenda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.closet.api.rest.dao.implementation.DaoUtil.prepareStatement;


/**
 * Created by jalberto.munoz on 02/05/2016.
 */
public class JDBCPrendaDao implements PrendaDao {
    private static final String SQL_FIND_GARMENT_BY_USER_ID =
            "Select * from prenda where usuario_id=";

    private static final String SQL_FIND_GARMENT_BY_TYPE =
            "Select * from prenda where tipo_prenda_id=";

    private static final String SQL_FIND_GARMENT_BY_SIZE =
            "Select * from prenda where talla_id=";

    private static final String Select_count =
            "SELECT * FROM prenda ORDER BY id DESC LIMIT 1";

    private static final String INSERT_NEW_GARMENT_FULL =
            "INSERT INTO prenda (descripcion, fecha_creacion, talla_id, marca_id, tipo_prenda_ofrecida, tipo_prenda_id, usuario_id, foto) " +
                    "VALUES (?, NOW(), ?, ?, ?, ?, ?, 'location')";

    private static final String INSERT_UPDATE_PHOTO_PATH = "UPDATE prenda SET foto = ?  WHERE id = ?";

    private static final String SQL_FIND_COMMENTS_BY_COMMENTID = "Select * from comentario where prenda_id=";

    private static final String INSERT_NEW_COMMENT = "INSERT INTO comentario (usuario_id, texto, prenda_id, fecha) VALUES(?, ?, ?, NOW())";

    private static final String SQL_DELETE_GARMENT = "DELETE FROM prenda WHERE id = ?";





    private DaoFactory daoFactory;

    JDBCPrendaDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public List<Prenda> findPrendasById(Long id) throws DaoException {
        List<Prenda> prendas = new ArrayList<Prenda>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_GARMENT_BY_USER_ID + id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                prendas.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return prendas;
    }

    @Override
    public void delete(Prenda prenda) throws DaoException {
        Object[] values = {
                prenda.getId()
        };

        try (

                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_DELETE_GARMENT, false, values);
        ) {

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DaoException("No se ha podido borrar el usuario");
            } else {
                prenda.setId(null);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Prenda> findPrendasByTipo(Long id) throws DaoException {
        List<Prenda> prendas = new ArrayList<Prenda>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_GARMENT_BY_TYPE + id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                prendas.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return prendas;
    }

    @Override
    public List<Prenda> findPrendasByTalla(String talla) throws DaoException {
        List<Prenda> prendas = new ArrayList<Prenda>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_GARMENT_BY_SIZE + talla);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                prendas.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return prendas;
    }

    @Override
    public int countNext(){
        int counter = 0;
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(Select_count);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
            counter = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return counter;
    }

    @Override
    public void insertGarment(Prenda prenda){
        Object[] values = {
                prenda.getDescripcion(),
                prenda.getTallaId(),
                prenda.getMarcaId(),
                prenda.getTipoPrendaOfrecida(),
                prenda.getTipoPrendaId(),
                prenda.getUsuarioId()
        };
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, INSERT_NEW_GARMENT_FULL, true, values);
        ) {

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DaoException("Creating user failed, no rows affected.");
            }
        }catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void updatePhoto(String path, int id){
        Object[] values = {
            path, id
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, INSERT_UPDATE_PHOTO_PATH, true, values);
        ) {

            statement.executeUpdate();
        }catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Comentario> getCommentsByGarmentId(Long id) throws DaoException{
        List<Comentario> comentarios = new ArrayList<Comentario>();

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_COMMENTS_BY_COMMENTID+id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                comentarios.add(mapComment(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return comentarios;
    }

    @Override
    public void insertComments(Comentario comentario){
        Object[] values = {
                comentario.getUsuarioId(),
                comentario.getTexto(),
                comentario.getPrendaId()
        };
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, INSERT_NEW_COMMENT, true, values);
        ) {

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DaoException("Creating user failed, no rows affected.");
            }
        }catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    private Prenda map(ResultSet resultSet) throws SQLException {
        Prenda prenda = new Prenda();
        prenda.setId(resultSet.getLong("id"));
        prenda.setDescripcion(resultSet.getString("descripcion"));
        prenda.setMarcaId(resultSet.getLong("marca_id"));
        prenda.setTallaId(resultSet.getLong("talla_id"));
        prenda.setFoto(resultSet.getString("foto"));
        prenda.setTipoPrendaOfrecida(resultSet.getLong("tipo_prenda_ofrecida"));
        prenda.setTipoPrendaId(resultSet.getLong("tipo_prenda_id"));
        prenda.setUsuarioId(resultSet.getLong("usuario_id"));
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date fechaCreacion = df.parse(resultSet.getString("fecha_creacion"));
            prenda.setFechaCreacion(fechaCreacion);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return prenda;
    }

    private Comentario mapComment(ResultSet resultSet) throws SQLException {
        Comentario comentario = new Comentario();
        comentario.setId(resultSet.getLong("id"));
        comentario.setFecha(resultSet.getDate("fecha"));
        comentario.setTexto(resultSet.getString("texto"));
        comentario.setUsuarioId(resultSet.getLong("usuario_id"));
        comentario.setPrendaId(resultSet.getLong("prenda_id"));
        return comentario;
    }
}
