package com.closet.api.rest.dao.interfaces;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.model.Comentario;
import com.closet.api.rest.model.Prenda;

import java.util.List;

/**
 * Created by jalberto.munoz on 02/05/2016.
 */
public interface PrendaDao {

    List<Prenda> findPrendasById(Long id) throws DaoException;

    List<Prenda> findPrendasByTipo (Long id) throws DaoException;

    List<Prenda> findPrendasByTalla (String talla) throws DaoException;

    int countNext() throws DaoException;

    void insertGarment(Prenda prenda) throws DaoException;

    void updatePhoto(String path, int id) throws DaoException;

    List<Comentario> getCommentsByGarmentId(Long id) throws DaoException;

    void insertComments(Comentario comentario) throws DaoException;

    void delete(Prenda prenda) throws DaoException;

}
