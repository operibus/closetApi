package com.closet.api.rest.dao.implementation;

/**
 * Created by Albert on 25/04/2016.
 */
import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.DaoProperties;
import com.closet.api.rest.dao.interfaces.*;
import com.closet.api.rest.model.Marca;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public abstract class DaoFactory {
    private static final String PROPERTY_URL = "url";
    private static final String PROPERTY_DRIVER = "driver";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_PASSWORD = "password";

    // Actions ------------------------------------------------------------------------------------

    /**
     * Returns a new DAOFactory instance for the given database name.
     * @param name The database name to return a new DAOFactory instance for.
     * @return A new DAOFactory instance for the given database name.
     * missing in the classpath or cannot be loaded, or if a required property is missing in the
     * properties file, or if either the driver cannot be loaded or the datasource cannot be found.
     */
    public static DaoFactory getInstance(String name) throws DaoException {
        if (name == null) {
            throw new DaoException("Database name is null.");
        }

        DaoProperties properties = new DaoProperties(name);
        String url = properties.getProperty(PROPERTY_URL, true);
        String driverClassName = properties.getProperty(PROPERTY_DRIVER, false);
        String password = properties.getProperty(PROPERTY_PASSWORD, false);
        String username = properties.getProperty(PROPERTY_USERNAME, password != null);
        DaoFactory instance;

        // If driver is specified, then load it to let it register itself with DriverManager.
        if (driverClassName != null) {
            try {
                Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                throw new DaoException(
                        "Driver class '" + driverClassName + "' is missing in classpath.", e);
            }
            instance = new DriverManagerDAOFactory(url, username, password);
        }

        // Else assume URL as DataSource URL and lookup it in the JNDI.
        else {
            DataSource dataSource;
            try {
                dataSource = (DataSource) new InitialContext().lookup(url);
            } catch (NamingException e) {
                throw new DaoException(
                        "DataSource '" + url + "' is missing in JNDI.", e);
            }
            if (username != null) {
                instance = new DataSourceWithLoginDAOFactory(dataSource, username, password);
            } else {
                instance = new DataSourceDAOFactory(dataSource);
            }
        }

        return instance;
    }


    abstract Connection getConnection() throws SQLException;


    public ProvinciaDao getProvinciaDao() {
        return new JDBCProvinciaDao(this);
    }

    public CiudadDao getCiudadDao() {
        return new JDBCCiudadDao(this);
    }

    public TipoPrendaDao getTipoPrendaDao() {
        return new JDBCTipoPrendaDao(this);
    }

    public UsuarioDao getUsuarioDao() {
        return new JDBCUsuarioDao(this);
    }

    public PrendaDao getPrendaDao() {
        return new JDBCPrendaDao(this);
    }

    public ConversacionDao getConversacionDao(){
        return new JDBCConversacionDao(this);
    }

    public TallaDao getTallaDao(){
        return new JDBCTallaDao(this);
    }

    public MessageDao getMessageDao(){
        return new JDBCMessageDao(this);
    }

    public MarcaDao getMarcaDao(){ return new JDBCMarcaDao(this); }



}

/**
 * The DriverManager based DAOFactory.
 */
class DriverManagerDAOFactory extends DaoFactory {
    private String url;
    private String username;
    private String password;

    DriverManagerDAOFactory(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}

/**
 * The DataSource based DAOFactory.
 */
class DataSourceDAOFactory extends DaoFactory {
    private DataSource dataSource;

    DataSourceDAOFactory(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}

/**
 * The DataSource-with-Login based DAOFactory.
 */
class DataSourceWithLoginDAOFactory extends DaoFactory {
    private DataSource dataSource;
    private String username;
    private String password;

    DataSourceWithLoginDAOFactory(DataSource dataSource, String username, String password) {
        this.dataSource = dataSource;
        this.username = username;
        this.password = password;
    }

    @Override
    Connection getConnection() throws SQLException {
        return dataSource.getConnection(username, password);
    }
}
