package com.closet.api.rest.dao.interfaces;

import com.closet.api.rest.model.Marca;

import java.util.List;

/**
 * Created by Albert on 24/05/2016.
 */
public interface MarcaDao {

    Marca getMarcaById(Long id);

    List<Marca> list();

}
