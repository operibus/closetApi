package com.closet.api.rest.dao.implementation;

import com.closet.api.rest.dao.interfaces.MessageDao;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.Mensaje;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by jalberto.munoz on 04/05/2016.
 */
public class JDBCMessageDao implements MessageDao {
    private DaoFactory daoFactory;

    JDBCMessageDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    private Mensaje map(ResultSet resultSet) throws SQLException {
        Mensaje mensaje = new Mensaje();
        mensaje.setId(resultSet.getLong("id"));
        mensaje.setUsuarioId(resultSet.getLong("usuario_id"));
        mensaje.setConversacionId(resultSet.getLong("conversacion_id"));
        mensaje.setTexto(resultSet.getString("texto"));
        return mensaje;
    }

}
