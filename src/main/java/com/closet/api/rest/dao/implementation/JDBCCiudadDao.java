package com.closet.api.rest.dao.implementation;
import com.closet.api.rest.dao.interfaces.CiudadDao;
import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.model.Ciudad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albert on 25/04/2016.
 */

public class JDBCCiudadDao implements CiudadDao {

    private static final String SQL_FIND_CIUDAD_PROVINCIA =
            "Select nombre, provincia_id, id from ciudad where provincia_id=";
    private static final String SQL_FIND_CIUDAD_BY_ID =
            "Select * from ciudad where id=";

    private DaoFactory daoFactory;

    JDBCCiudadDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public List<Ciudad> getAllCitiesForProvincia(Long id) throws DaoException {
        List<Ciudad> ciudades = new ArrayList<Ciudad>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_CIUDAD_PROVINCIA+id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                ciudades.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return ciudades;
    }

    public  Ciudad getCiudadById(Long id){
        Ciudad ciudad = new Ciudad();

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_FIND_CIUDAD_BY_ID+id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                ciudad = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return ciudad;

    }

    private Ciudad map(ResultSet resultSet) throws SQLException {
        Ciudad ciudad = new Ciudad();
        ciudad.setId(resultSet.getLong("id"));
        ciudad.setNombre(resultSet.getString("nombre"));
        ciudad.setProvinciaId(resultSet.getLong("provincia_id"));
        return ciudad;
    }
}
