package com.closet.api.rest.dao.implementation;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.interfaces.ConversacionDao;
import com.closet.api.rest.model.Conversacion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jalberto.munoz on 03/05/2016.
 */
public class JDBCConversacionDao implements ConversacionDao {

    private DaoFactory daoFactory;

    JDBCConversacionDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public List<Conversacion> getConversacionesByUserId(Long id) throws DaoException {
        List<Conversacion> conversaciones = new ArrayList<Conversacion>();
        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement("Select * from conversacion where usuario_id="+id+" or usuario2_id="+id);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                conversaciones.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return conversaciones;
    }

    private Conversacion map(ResultSet resultSet) throws SQLException {
        Conversacion conversacion = new Conversacion();
        conversacion.setId(resultSet.getLong("id"));
        conversacion.setUsuarioId(resultSet.getLong("usuario_id"));
        conversacion.setUsuario2Id(resultSet.getLong("usuario2_id"));
        return conversacion;
    }
}
