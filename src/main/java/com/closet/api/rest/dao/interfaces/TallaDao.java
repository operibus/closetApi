package com.closet.api.rest.dao.interfaces;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.model.Talla;

import java.util.List;

/**
 * Created by Albert on 19/05/2016.
 */
public interface TallaDao {
    List<Talla> list() throws DaoException;

    Talla getTallaById(Long id) throws DaoException;
}
