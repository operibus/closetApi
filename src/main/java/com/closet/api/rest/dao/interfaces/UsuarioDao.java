package com.closet.api.rest.dao.interfaces;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.Provincia;
import com.closet.api.rest.model.Usuario;

import java.util.List;

/**
 * Created by jalberto.munoz on 27/04/2016.
 */
public interface UsuarioDao {

    List<Usuario> usersFind() throws DaoException;

    List<Usuario> usersFindByCity(Long id) throws DaoException;

    Usuario findUserByEmailPass(Usuario usuario) throws DaoException;

    void create(Usuario usuario) throws IllegalArgumentException, DaoException;

    void update(Usuario usuario) throws IllegalArgumentException, DaoException;

    void delete(Usuario usuario) throws DaoException;

    boolean existEmail(String email) throws DaoException;

    Usuario getUserById(Long id) throws DaoException;

    List<Usuario> getUsersByProvincia(List<Ciudad> ciudades);

//    public void changePassword(Usuario user) throws DaoException;

    public void updatePhoto(String photoPath, Long id);

}
