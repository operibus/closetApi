package com.closet.api.rest.controller;

import com.closet.api.rest.dao.implementation.DaoFactory;
import com.closet.api.rest.dao.interfaces.PrendaDao;
import com.closet.api.rest.dao.interfaces.UsuarioDao;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Albert on 25/05/2016.
 */
@Service
public class ImageController {

    public void putImageGarment(byte[] file){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        PrendaDao prendaDao = javabase.getPrendaDao();
        int id = prendaDao.countNext();
        String photoPath = "E:/images/"+id+".jpg";
        byte[] data = Base64.decodeBase64(file);
        try (OutputStream stream = new FileOutputStream(photoPath)) {
            stream.write(data);
        }catch(Exception e){

        }
        prendaDao.updatePhoto(photoPath, id);

    }


    public void putImageProfile(byte[] file, Long id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao userDao = javabase.getUsuarioDao();
        String photoPath = "E:/images/profile-"+id+".jpg";
        byte[] data = Base64.decodeBase64(file);
        try (OutputStream stream = new FileOutputStream(photoPath)) {
            stream.write(data);
        }catch(Exception e){

        }
        userDao.updatePhoto(photoPath, id);

    }



}
