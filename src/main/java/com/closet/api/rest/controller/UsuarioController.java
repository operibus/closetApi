package com.closet.api.rest.controller;

import com.closet.api.rest.config.DaoException;
import com.closet.api.rest.dao.implementation.DaoFactory;
import com.closet.api.rest.dao.interfaces.CiudadDao;
import com.closet.api.rest.dao.interfaces.ProvinciaDao;
import com.closet.api.rest.dao.interfaces.UsuarioDao;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.Profile;
import com.closet.api.rest.model.Provincia;
import com.closet.api.rest.model.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jalberto.munoz on 29/04/2016.
 */

@Service
public class UsuarioController {
    public List<Usuario> getAllUsers() {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        List<Usuario> usuarios = usuarioDao.usersFind();
        return usuarios;
    }

    public Usuario addNewUser(Usuario usuario) {
        String success;
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        Boolean existe = this.existEmail(usuario.getEmail());
        if (existe) {
            success = "error";
            usuario.setNombre(success);
        } else {
            usuarioDao.create(usuario);
        }
        return usuario;
    }

    public void updateUser(Usuario usuario) {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        CiudadDao ciudadDao = javabase.getCiudadDao();
        ProvinciaDao provinciaDao = javabase.getProvinciaDao();
        Ciudad ciudad = new Ciudad();
        Provincia provincia = new Provincia();
        usuarioDao.update(usuario);
    }

    public void deleteUser(Long id) {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        Usuario usuario = new Usuario();
        usuario.setId(id);
        usuarioDao.delete(usuario);
    }

    public Profile getUserByEmail(String email, String password) {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        CiudadDao ciudadDao = javabase.getCiudadDao();
        ProvinciaDao provinciaDao = javabase.getProvinciaDao();
        Ciudad ciudad = new Ciudad();
        Provincia provincia = new Provincia();
        Usuario usuario = new Usuario();
        usuario.setEmail(email);
        usuario.setContrasena(password);
        usuario = usuarioDao.findUserByEmailPass(usuario);
        Profile profile = new Profile();
        profile.setId(usuario.getId());
        ciudad = ciudadDao.getCiudadById(usuario.getCiudadId());
        provincia = provinciaDao.getProvinciaById(ciudad.getProvinciaId());
        profile.setProvincia(provincia);
        profile.setTipoUsuario(usuario.getTipoUsuarioId());
        profile.setCiudad(ciudad);
        profile.setEmail(usuario.getEmail());
        profile.setNombre(usuario.getNombre());


        return profile;
    }

    public Boolean existEmail(String email) {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        boolean existe = usuarioDao.existEmail(email);
        return existe;
    }

    public List<Usuario> getUsersByCity(String id) {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        List<Usuario> usuarios = usuarioDao.usersFindByCity(Long.parseLong(id));
        return usuarios;
    }

    public Usuario getUsuarioById(String id) {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        Usuario usuario = new Usuario();
        return usuario;
    }
}
