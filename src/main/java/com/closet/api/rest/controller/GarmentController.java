package com.closet.api.rest.controller;

import com.closet.api.rest.dao.implementation.DaoFactory;
import com.closet.api.rest.dao.interfaces.*;
import com.closet.api.rest.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albert on 25/05/2016.
 */
@Service
public class GarmentController {

    public List<Talla> getTallas(){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        TallaDao tallaDao = javabase.getTallaDao();
        List<Talla> tallas = tallaDao.list();
        return tallas;
    }

    public List<TipoPrenda> getTipoPrenda(){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        TipoPrendaDao tipoPrendaDao = javabase.getTipoPrendaDao();
        List<TipoPrenda> tipoPrendas = tipoPrendaDao.list();
        return tipoPrendas;
    }

    public List<Marca> getMarca(){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        MarcaDao marcaDao = javabase.getMarcaDao();
        List<Marca> marcas = marcaDao.list();
        return marcas;
    }

    public void postGarment(Prenda prenda){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        PrendaDao prendaDao = javabase.getPrendaDao();
        prendaDao.insertGarment(prenda);
    }

    public List<Comment> getCommentsByGarmentId(String id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        PrendaDao prendaDao = javabase.getPrendaDao();
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        List<Comentario> comentarios = new ArrayList<>();
        comentarios = prendaDao.getCommentsByGarmentId(Long.parseLong(id));
        List<Comment> comentariosDev = new ArrayList<Comment>();
        for (int i = 0; i< comentarios.size(); i++){
            Comment comment = new Comment();
            comment.setId(comentarios.get(i).getId());
            comment.setFecha(comentarios.get(i).getFecha());
            comment.setTexto(comentarios.get(i).getTexto());
            comment.setPrendaId(comentarios.get(i).getPrendaId());
            Usuario usuario = new Usuario();
            usuario = usuarioDao.getUserById(comentarios.get(i).getUsuarioId());
            comment.setUsuarioId(usuario.getNombre());
            comentariosDev.add(comment);
        }
        return comentariosDev;
    }

    public void insertComment(Comentario comentario){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        PrendaDao prendaDao = javabase.getPrendaDao();
        prendaDao.insertComments(comentario);

    }

    public void deleteGarment(Long id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        PrendaDao prendaDao = javabase.getPrendaDao();
        Prenda prenda = new Prenda();
        prenda.setId(id);
        prendaDao.delete(prenda);
    }

}
