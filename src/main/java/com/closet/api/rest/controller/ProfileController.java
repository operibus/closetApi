package com.closet.api.rest.controller;

import com.closet.api.rest.dao.implementation.DaoFactory;
import com.closet.api.rest.dao.interfaces.*;
import com.closet.api.rest.model.*;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jalberto.munoz on 02/05/2016.
 */

@Service
public class ProfileController {


    public List<PrendaProfile> getPrendasByUserId(String id) throws Exception{
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        PrendaDao prendaDao = javabase.getPrendaDao();
        List<Prenda> prendas = prendaDao.findPrendasById(Long.parseLong(id));
        List<PrendaProfile> profiles = new ArrayList<>();
        for(int i=0; i<prendas.size(); i++){
            Marca marca = new Marca();
            Talla talla = new Talla();
            TipoPrenda tipoPrenda = new TipoPrenda();
            MarcaDao marcaDao =  javabase.getMarcaDao();
            marca = marcaDao.getMarcaById(prendas.get(i).getMarcaId());
            TallaDao tallaDao = javabase.getTallaDao();
            talla = tallaDao.getTallaById(prendas.get(i).getTallaId());
            TipoPrendaDao tipoPrendaDao = javabase.getTipoPrendaDao();
            tipoPrenda = tipoPrendaDao.getTipoPrendaById(prendas.get(i).getTipoPrendaId());
            PrendaProfile prendaProfile = new PrendaProfile();
            prendaProfile.setFoto(encodeFileToBase64Binary(prendas.get(i).getFoto()));
            prendaProfile.setId(prendas.get(i).getId());
            prendaProfile.setDescripcion(prendas.get(i).getDescripcion());
            prendaProfile.setTallaId(talla.getNombre());
            prendaProfile.setTipoPrenda(tipoPrenda.getNombre());
            prendaProfile.setMarca(marca.getNombre());
            if(prendas.get(i).getTipoPrendaOfrecida() == prendas.get(i).getTipoPrendaId()){
                prendaProfile.setTipoPrendaOfrecida(tipoPrenda.getNombre());
            }else{
                tipoPrenda = tipoPrendaDao.getTipoPrendaById(prendas.get(i).getTipoPrendaOfrecida());
                prendaProfile.setTipoPrendaOfrecida(tipoPrenda.getNombre());
            }
            profiles.add(prendaProfile);
        }
        return profiles;
    }


    public Profile getUser(String id) {
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        UsuarioDao userDao = javabase.getUsuarioDao();
        Profile userProfile = new Profile();
        ProvinciaDao provinciaDao = javabase.getProvinciaDao();
        CiudadDao ciudadDao = javabase.getCiudadDao();
        Usuario usuario = new Usuario();
        usuario = userDao.getUserById(Long.parseLong(id));
        Ciudad ciudad = new Ciudad();
        Provincia provincia = new Provincia();
        ciudad = ciudadDao.getCiudadById(usuario.getCiudadId());
        provincia = provinciaDao.getProvinciaById(ciudad.getProvinciaId());
        userProfile.setNombre(usuario.getNombre());
        userProfile.setEmail(usuario.getEmail());
        userProfile.setId(usuario.getId());
        userProfile.setCiudad(ciudad);
        userProfile.setTipoUsuario(usuario.getTipoUsuarioId());
        userProfile.setProvincia(provincia);
        return userProfile;
    }
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }
    private String encodeFileToBase64Binary(String fileName)
            throws IOException {

        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        byte[] encoded = Base64.encodeBase64(bytes);
        String encodedString = new String(encoded);
        return encodedString;
    }

}
