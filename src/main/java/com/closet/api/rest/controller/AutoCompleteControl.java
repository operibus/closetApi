package com.closet.api.rest.controller;

import com.closet.api.rest.dao.implementation.DaoFactory;
import com.closet.api.rest.dao.interfaces.*;
import com.closet.api.rest.model.*;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albert on 18/05/2016.
 */
@Service
public class AutoCompleteControl {

    public AutoComplete getAll(){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        ProvinciaDao provinciaDao = javabase.getProvinciaDao();
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        TipoPrendaDao tipoDao = javabase.getTipoPrendaDao();
        TallaDao tallaDao = javabase.getTallaDao();


        List<Provincia> provincias = provinciaDao.list();
        List <Usuario> usuarios = usuarioDao.usersFind();
        List<TipoPrenda> tipo = tipoDao.list();
        List<Talla> tallas = tallaDao.list();

        AutoComplete autoComplete = new AutoComplete();
        autoComplete.setProvincias(provincias);
        autoComplete.setUsuarios(usuarios);
        autoComplete.setTipoPrenda(tipo);
        autoComplete.setTallas(tallas);
        return autoComplete;
    }

    public List<PrendaProfile> getPrendaByTipo(String id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        PrendaDao prendaDao = javabase.getPrendaDao();
        List<Prenda> prendas = prendaDao.findPrendasByTipo(Long.parseLong(id));
        List<PrendaProfile> prendasProfile = new ArrayList<PrendaProfile>();
        for (int i = 0; i<prendas.size(); i++){
            try{
                prendas.get(i).setFoto(encodeFileToBase64Binary(prendas.get(i).getFoto()));

            }catch (Exception e){

            }
            Marca marca = new Marca();
            Talla talla = new Talla();
            TipoPrenda tipoPrenda = new TipoPrenda();
            MarcaDao marcaDao =  javabase.getMarcaDao();
            marca = marcaDao.getMarcaById(prendas.get(i).getMarcaId());
            TallaDao tallaDao = javabase.getTallaDao();
            talla = tallaDao.getTallaById(prendas.get(i).getTallaId());
            TipoPrendaDao tipoPrendaDao = javabase.getTipoPrendaDao();
            tipoPrenda = tipoPrendaDao.getTipoPrendaById(prendas.get(i).getTipoPrendaId());
            UsuarioDao usuarioDao = javabase.getUsuarioDao();
            Usuario usuario = new Usuario();
            usuario = usuarioDao.getUserById(prendas.get(i).getUsuarioId());
            PrendaProfile prendaProfile = new PrendaProfile();
            prendaProfile.setUsuario(usuario.getNombre());
            prendaProfile.setFoto(prendas.get(i).getFoto());
            prendaProfile.setId(prendas.get(i).getId());
            prendaProfile.setDescripcion(prendas.get(i).getDescripcion());
            prendaProfile.setTallaId(talla.getNombre());
            prendaProfile.setTipoPrenda(tipoPrenda.getNombre());
            prendaProfile.setMarca(marca.getNombre());
            if(prendas.get(i).getTipoPrendaOfrecida() == prendas.get(i).getTipoPrendaId()){
                prendaProfile.setTipoPrendaOfrecida(tipoPrenda.getNombre());
            }else{
                tipoPrenda = tipoPrendaDao.getTipoPrendaById(prendas.get(i).getTipoPrendaOfrecida());
                prendaProfile.setTipoPrendaOfrecida(tipoPrenda.getNombre());
            }
            prendasProfile.add(prendaProfile);
        }

        return prendasProfile;
    }

    public List<Usuario> getUserByProvince(String id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        CiudadDao ciudadDao = javabase.getCiudadDao();
        List<Ciudad> ciudades = ciudadDao.getAllCitiesForProvincia(Long.parseLong(id));
        UsuarioDao usuarioDao = javabase.getUsuarioDao();
        List<Usuario> usuarios= usuarioDao.getUsersByProvincia(ciudades);
        return usuarios;
    }

    public List<Profile> getUserById(String id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        CiudadDao ciudadDao = javabase.getCiudadDao();
        ProvinciaDao provinciaDao = javabase.getProvinciaDao();
        UsuarioDao usuariodDao = javabase.getUsuarioDao();
        Usuario usuario = usuariodDao.getUserById(Long.parseLong(id));
        List<Profile> profiles = new ArrayList();
            Ciudad ciudad = new Ciudad();
            Provincia provincia = new Provincia();
            Profile profile = new Profile();
            profile.setId(usuario.getId());
            ciudad = ciudadDao.getCiudadById(usuario.getCiudadId());
            provincia = provinciaDao.getProvinciaById(ciudad.getProvinciaId());
            profile.setProvincia(provincia);
            profile.setCiudad(ciudad);
            profile.setEmail(usuario.getEmail());
            profile.setNombre(usuario.getNombre());
        profiles.add(profile);
        return profiles;
    }

    public List<Prenda> getprendaByTalla(String nombre){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        PrendaDao prendaDao = javabase.getPrendaDao();
        List<Prenda> prendas = prendaDao.findPrendasByTalla(nombre);
        for (int i = 0; i<prendas.size(); i++){
            try{
                prendas.get(i).setFoto(encodeFileToBase64Binary(prendas.get(i).getFoto()));
            }catch (Exception e){

            }
        }
        return prendas;
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }
    private String encodeFileToBase64Binary(String fileName)
            throws IOException {

        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        byte[] encoded = Base64.encodeBase64(bytes);
        String encodedString = new String(encoded);
        return encodedString;
    }
}
