package com.closet.api.rest.controller;

import com.closet.api.rest.dao.implementation.DaoFactory;
import com.closet.api.rest.dao.interfaces.CiudadDao;
import com.closet.api.rest.dao.interfaces.ProvinciaDao;
import com.closet.api.rest.model.Ciudad;
import com.closet.api.rest.model.Provincia;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jalberto.munoz on 29/04/2016.
 */
@Service
public class FormController {
    public List<Provincia> getProvincias(){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        ProvinciaDao provinciaDao = javabase.getProvinciaDao();
        List<Provincia> provincias = provinciaDao.list();
        return provincias;
    }
    public List<Ciudad> getCitiesByProvincia(Long id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        CiudadDao ciudadDao = javabase.getCiudadDao();
        List<Ciudad> ciudades = ciudadDao.getAllCitiesForProvincia(id);
        return ciudades;
    }
}
