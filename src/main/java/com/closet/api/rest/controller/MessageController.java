package com.closet.api.rest.controller;

import com.closet.api.rest.dao.implementation.DaoFactory;
import com.closet.api.rest.dao.interfaces.ConversacionDao;
import com.closet.api.rest.dao.interfaces.MessageDao;
import com.closet.api.rest.model.Conversacion;
import com.closet.api.rest.model.Mensaje;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jalberto.munoz on 03/05/2016.
 */
@Service
public class MessageController {
    public List<Conversacion> getConversacionesByUserId(String id){
        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
        ConversacionDao conversacionDao = javabase.getConversacionDao();
        List<Conversacion> conversaciones = conversacionDao.getConversacionesByUserId(Long.parseLong(id));
        return conversaciones;
    }
//    public List<Mensaje> getMensajeByConversacionId(String id){
//        DaoFactory javabase = DaoFactory.getInstance("javabase.jdbc");
//        MessageDao mensajeDao = javabase.get();
//        List<Mensaje> mensajes = mensajeDao.getMensajeByConversationId(Long.parseLong(id));
//        return mensajes;
//    }
}
